package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.PropertiesManager;
import com.telerikacademy.finalproject.utils.RequestHandler;
import com.telerikacademy.finalproject.utils.UserActions;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import io.github.bonigarcia.wdm.managers.FirefoxDriverManager;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.*;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.dnd.DropTargetDragEvent;
import java.util.Calendar;
import java.util.Date;

public class HealthyFoodTests extends BaseTest {


    public void loginInApp(){
        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.signInButton);
        actions.clickElement(navPage.userName);
        actions.typeValueInField("stage.10team.gergana.svetoslav@gmail.com", navPage.userName);
        actions.clickElement(navPage.passwordField);
        actions.typeValueInField("10^ShangriL@!", navPage.passwordField);
        actions.clickElement(navPage.logInButton);
}
    @BeforeClass
    public static void classInit() {
    }

    @Before
    public void TestInitialize() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());
    }




        @Test
    public void navigatedToRegistrationFieldAndRegistration(){

        NavigationPage loginPage = new NavigationPage();
        actions.clickElement(loginPage.signInButton);
        actions.clickElement(loginPage.registrationButton);
        actions.clickElement(loginPage.regUserName);
        actions.typeValueInField("pile_oriz@abv.bg",loginPage.regUserName);
        actions.clickElement(loginPage.regPassword);
        actions.typeValueInField("Pileto123@",loginPage.regPassword);
        actions.clickElement(loginPage.regConfPass);
        actions.typeValueInField("Pileto123@", loginPage.regConfPass);
        actions.clickElement(loginPage.regFirsName);
        actions.typeValueInField("Pile", loginPage.regFirsName);
        actions.clickElement(loginPage.regLastName);
        actions.typeValueInField("Oriz", loginPage.regLastName);
        actions.clickElement(loginPage.regPictVisible);
        actions.clickElement(loginPage.pubVisPic);


     }
    @Test
    public void navigatedToLoginPageAndLogIn() {

        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.signInButton);
        actions.clickElement(navPage.userName);
        actions.typeValueInField("stage.10team.gergana.svetoslav@gmail.com", navPage.userName);
        actions.clickElement(navPage.passwordField);
        actions.typeValueInField("10^ShangriL@!", navPage.passwordField);
        actions.clickElement(navPage.logInButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.logOutButton);
        actions.clickElement(navPage.logOutButton);
    }

    @Test
    public void navigateToHome_UsingNavigation() {

        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.homeButton);
    }

    @Test
    public void registeredUserSendsAFriendRequest() {
     loginInApp();
        NavigationPage navPage = new NavigationPage();

        actions.clickElement(navPage.userScroll);
        actions.clickElement(navPage.moreButton);
        actions.isElementPresentUntilTimeout(navPage.invitationFriend,10);
        actions.clickElement(navPage.invitationFriend);
        actions.clickElement(navPage.connectButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.sentRequestReject);
        actions.clickElement(navPage.logOutButton);
    }
    @Test
    public void registeredUserRejectAFriendRequest() {
    loginInApp();
        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.userScroll);
        actions.clickElement(navPage.moreButton);
        actions.isElementPresentUntilTimeout(navPage.invitationFriend,10);
        actions.clickElement(navPage.invitationFriend);
        actions.clickElement(navPage.sentRequestReject);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.connectButton);
        actions.clickElement(navPage.logOutButton);
    }
    @Test
    public void registeredUserCanAcceptRequests(){
      loginInApp();
        NavigationPage navPage = new NavigationPage();

        actions.clickElement(navPage.userScroll);
        actions.isElementPresentUntilTimeout(navPage.invitationFriend,10);
        actions.clickElement(navPage.invitationFriend);
        actions.clickElement(navPage.connectButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.disconnectFriend);
    }

    @Test
    public void userDeclinesTheFriendRequest(){
      loginInApp();
        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.userScroll);
        actions.isElementPresentUntilTimeout(navPage.invitationFriend,10);
        actions.clickElement(navPage.invitationFriend);
        actions.clickElement(navPage.rejectFriendRequest);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.connectButton);
    }
}

