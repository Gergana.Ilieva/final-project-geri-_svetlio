package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {
    public NavigationPage() {
        super("base.url");
    }

    public final String homeButton = "navigation.Home";
    public final String logOutButton = "navigation.LogOut";
    public final String signInButton = "navigation.LogIn";
    public final String userName = "logIn.Name";
    public final String passwordField = "logIn.Password";
    public final String logInButton = "logIn.Button";
    public final String registrationButton = "navigation.Register";
    public final String regUserName = "register.Username(Email)";
    public final String regPassword = "register.Password";
    public final String regConfPass = "register.ConfirmPassword";
    public final String regFirsName = "register.FirstName";
    public final String regLastName = "register.LastName";
    public final String regPictButton = "register.ProfilePictureButton";
    public final String regPictVisible = "register.ProfilePictureVisibility";
    public final String regGender = "register.Gender";
    public final String genderMale = "register.GenderMale";
    public final String genderFemale = "register.GenderFemale";
    public final String regNationality = "register.Nationality";
    public final String regAboutYou = "register.AboutYou";
    public final String regAge = "register.Age";
    public final String regButton = "registerButton";
    public final String pubVisPic = "register.PublicVisiblePicture";
    public final String connVisPic = "register.ConnectionVisiblePicture";
    public final String userScroll = "users.Scroll";
    public final String requestPage = "users.Requests";
    public final String friendsPage = "users.Friends";
    public final String userPage = "users.Users";
    public final String connectButton = "connections.ConnectButton";
    public final String userFriend = "connections.UserFriend";
    public final String sendConnectButton = "connections.SendConnectButton";
    public final String confirmButton = "connection.ConfirmFriendButton";
    public final String moreButton = "user.More";
    public final String invitationFriend = "user.InvitationFromAFriend";
    public final String sentRequestReject = "user.SentRequestReject";
    public final String disconnectFriend = "user.DisconnectFriends";
    public final String rejectFriendRequest = "user.RejectFriendRequest";
}